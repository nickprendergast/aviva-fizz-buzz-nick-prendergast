<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>

	<form:form action="output" method="POST"
		modelAttribute="FizzBuzzNumber">
	Enter value between 1-1000
  	   <form:input type="number" path="value" />
  	   <form:errors path="value" />
		<input type="submit" value="Submit" />
	</form:form>
</body>
</html>
