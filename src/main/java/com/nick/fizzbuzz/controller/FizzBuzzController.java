package com.nick.fizzbuzz.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nick.fizzbuzz.service.FizzBuzzService;
import com.nick.fizzbuzz.transferobject.FizzBuzzNumber;

/**
 * Handles requests for the application home page.
 */
@Controller
public class FizzBuzzController {

	@Autowired
	private FizzBuzzService fizzBuzzService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String processFizzBuzz(Model model) {
		model.addAttribute("FizzBuzzNumber", new FizzBuzzNumber());
		return "home";
	}

	@RequestMapping(value = "/output", method = RequestMethod.POST)
	public String processFizzBuzz(
			@Valid @ModelAttribute("FizzBuzzNumber") FizzBuzzNumber fizzBuzzNumber, BindingResult binding, Model model) {
		if (binding.hasErrors()) {
			return "home";
		}
		int pickedValue = fizzBuzzNumber.getValue();
		model.addAttribute("pickedValue", pickedValue);
		HashMap<Integer, String> fizzBuzzList = fizzBuzzService.getFizzBuzzList(pickedValue);
		model.addAttribute("fizzBuzzList", fizzBuzzList);

		// returns the result page
		return "result";
	}

}
