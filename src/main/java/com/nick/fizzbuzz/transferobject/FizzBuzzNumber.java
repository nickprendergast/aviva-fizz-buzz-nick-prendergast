package com.nick.fizzbuzz.transferobject;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class FizzBuzzNumber {

	@Min(value = 1, message = "Value must be between 1 - 1000")
	@Max(value = 1000, message = "Value must be between 1 - 1000")
	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	
	
}
