package com.nick.fizzbuzz.service;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {

	@Override
	public HashMap<Integer, String> getFizzBuzzList(int numberPicked) {
		HashMap<Integer, String> data = new HashMap<Integer, String>();
		for (int i = 1; i <= numberPicked; i++) {
			if (i % 15 == 0) {
				data.put(i, "Fizz Buzz");
			} else if (i % 3 == 0) {
				data.put(i, "fizz");
			} else if (i % 5 == 0) {
				data.put(i, "buzz");
			} else {
				data.put(i, String.valueOf(i));
			}
		}
		return data;
	}

}
