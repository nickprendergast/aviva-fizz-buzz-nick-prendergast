package com.nick.fizzbuzz.service;

import java.util.HashMap;

public interface FizzBuzzService {

	public HashMap<Integer, String> getFizzBuzzList(int numberPicked);

}
